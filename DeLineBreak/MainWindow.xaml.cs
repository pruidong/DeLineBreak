﻿
using System.Windows;


namespace DeLineBreak
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 删除不可见字符
        /// </summary>
        /// <param name="sourceString">原始字符</param>
        /// <returns>删除后结果</returns>
        /// 参考:https://blog.csdn.net/dingxingmei/article/details/7738548
        public static string DeleteUnVisibleChar(string sourceString)
        {
            System.Text.StringBuilder sBuilder = new System.Text.StringBuilder(131);
            for (int i = 0; i < sourceString.Length; i++)
            {
                int Unicode = sourceString[i];
                if (Unicode >= 16)
                {
                    sBuilder.Append(sourceString[i].ToString());
                }
            }
            return sBuilder.ToString();
        }


        private void replace_Click(object sender, RoutedEventArgs e)
        {
            contentTextBox.Text = DeleteUnVisibleChar(contentTextBox.Text);
            if (Properties.Settings.Default.AutoClipboard)
            {
                Clipboard.SetText(contentTextBox.Text);
            }
        }

        private void clear_Click(object sender, RoutedEventArgs e)
        {
            contentTextBox.Text = "";
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void autoSettingClipboard_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.AutoClipboard = true;
            Properties.Settings.Default.Save();
        }

        private void autoSettingClipboard_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.AutoClipboard = false;
            Properties.Settings.Default.Save();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            autoSettingClipboard.IsChecked = Properties.Settings.Default.AutoClipboard;
        }
    }
}
